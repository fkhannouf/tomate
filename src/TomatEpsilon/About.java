package TomatEpsilon;


import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * About.java
 *
 * Created on 30 janv. 2010, 17:46:53
 */

/**
 *
 * @author fkhannouf
 */
public class About extends JPanel implements ActionListener, ControlContext {

    /** Creates new form About */
    public About() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();

        jPanel1.setMaximumSize(new java.awt.Dimension(720, 433));
        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(720, 433));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/myTomate.png"))); // NOI18N
        jLabel2.setText("The Original Moophy's Audio Toolbox ε");

        jTextPane1.setContentType("text/html");
        jTextPane1.setText("<html>\n  <head>\n\n  </head>\n  <body>\n    <p style=\"margin-top: 0\">\n      A very experimental Freeware brought to you by :<br/><br/>\nMoophy - <a href='http://www.melophony.com'>http://www.melophony.com</a><br/>\n<br/>\nIcon by Chrisdesign - <a href='http://chrisdesign.wordpress.com/'>http://chrisdesign.wordpress.com/</a><br/>\n<br/>\nThis product includes software developed by the\n      JDOM Project ( <a href='http://www.jdom.org/'>http://www.jdom.org/)</a>.<br/>\n Copyright (C) 2000-2004 Jason Hunter & Brett McLaughlin. All rights reserved.<br/>\n<hr/>\nDISCLAIMER<br/>\nThere is NO WARRANTY for this program. The copyright holder provides this program “AS IS” without warranty of any kind. I assumed no responsibility if your computer explodes, or if your dog bites you because of \"Tomate\".<br/>You've been warned! :-)<br/>\n<br/>\nHISTORY<br/>\nAs a Renoise user, I miss some little automations specially about creating instruments.<br/>\nSo I decided to make a little utility to make my life easier. It's a Java application so it should run fine on all major platforms.<br/>\n<br/>\nI hope you'll enjoy it!<br/><br/>\n<u>Moophy</u><br/>\n    </p>\n  </body>\n</html>\n");
        jScrollPane1.setViewportView(jTextPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables

    public static void main(String s[]) {
        // Instanciation of our class
        About aboutPanel = new About();
        aboutPanel.open();

        // Creation of a new frame
        JFrame f = new JFrame("About");
        // Handling of close event
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) { System.exit(0); }
        });
        f.getContentPane().add("Center", aboutPanel);
        f.pack();
        Misc.initComponentSize(f);
    }

    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void open() {
        Misc.initComponentSize(this);
    }

    public void close() {
        //throw new UnsupportedOperationException("Not supported yet.");
    }


}
